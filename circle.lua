function circle(diameter)
	if diameter%2 == 0 then
		radius = (diameter-2)/2
	else
		radius = (diameter-1)/2
	end
	
	fullset = {}
	set1 = {}
	set2 = {}
	set3 = {}
	set4 = {}
	set5 = {}
	set6 = {}
	set7 = {}
	set8 = {}
	x = 0; --x coord of block to be placed
	y = radius; --y coord of block to be placed
	p = (5-radius*4)/4 
	
	--GENERATE POINTS--
	while x<y do
		if x == 0 then
			table.insert(set1,{0,y})
			table.insert(set4,{0,-y})
			table.insert(set7,{y,0})
			table.insert(set6,{-y,0})
		elseif x == y then
			table.insert(set5,{x,y})
			table.insert(set2,{-x,y})
			table.insert(set3,{x,-y})
			table.insert(set8,{-x,-y})
		elseif x<y then
			table.insert(set1,{0+x,0+y})
			table.insert(set2,{0-x,0+y})
			table.insert(set3,{0+x,0-y})
			table.insert(set4,{0-x,0-y})		
			table.insert(set5,{0+y,0+x})
			table.insert(set6,{0-y,0+x})
			table.insert(set7,{0+y,0-x})
			table.insert(set8,{0-y,0-x})
		end
		x = x+1
		if p < 0 then
			p = p+ 2*x+1
		else
			y = y-1
			p = p+ 2*(x-y)+1
		end
	end
	
	--Manipulate coords and concatenate tables--
	for i=1,8 do
			--put coordinates in order for block placement--
		if i==1 then a = set1
			elseif i==2 then a = set5 a = tabmod.reverseTab(a)
			elseif i==3 then a = set7
			elseif i==4 then a = set3 a = tabmod.reverseTab(a)
			elseif i==5 then a = set4
			elseif i==6 then a = set8 a = tabmod.reverseTab(a)
			elseif i==7 then a = set6
			elseif i==8 then a = set2 a = tabmod.reverseTab(a)
		end
			--translate quadrants for even diameter--
		if diameter%2 == 0 and (i == 3 or i == 4 or i == 5 or i == 6) then
			for k,v in pairs(a) do
				a[k] = {v[1],v[2]-1}
			end
			if i == 3 then
				table.insert(a,1,{(a[1][1]),a[1][2]+1})
			end
			if i == 5 then
				table.insert(a,1,{(a[1][1]+1),a[1][2]})
			end
		end
		if diameter%2 == 0 and (i == 7 or i == 8 or i == 5 or i == 6) then
			for k,v in pairs(a) do
				a[k] = {v[1]-1,v[2]}
			end
			if i == 8 then
				table.insert(a,{(a[#a][1]+1),a[#a][2]})
			end
			if i == 6 then
				table.insert(a,{a[#a][1],a[#a][2]+1})
			end
		end
			--concatenate finished tables--
		for k,v in pairs(a) do
			table.insert(fullset,v)
		end
		print(i)
	end
			--return list of coordinates in circle--
	return fullset
end

function printPoints(points)
	cx=0
	cy=0
	cz=0
	face = 1
	for k,v in pairs(points) do
		cx,cy,cz,face = coords.moveTo(cx,cy,cz,v[1],v[2],1,face)
		turtle.placeDown()
	end
end


------------------------------------------------------------------------
print("Enter the desired diameter: ")
diameter = tonumber(read())
points = circle(diameter)
printPoints(points)


