


function accuMove(direction)
	move = false
	while move == false do
		if direction == "forward" then
			move = turtle.forward()
		elseif direction == "down" then
			move = turtle.down()
		elseif direction == "up" then
			move = turtle.up()
		end
		if move == false then
			print("Move failed. Remove obstruction.")
			sleep(5)
			resetScreen()
		end
	end
end

function moveTo(cx,cy,cz,x,y,z, face)
	--Move in z axis
	if cz < z then
		while cz < z do
			accuMove("up")
			cz = cz+1
		end
	elseif cz > z then
		while cz > z do
			accuMove("down")
			cz = cz-1
		end
	end
	--Move in x axis
	if cx < x then
		if face == 1 then
			turtle.turnRight()
		elseif face == 2 then
			turtle.turnRight()
			turtle.turnRight()
		elseif face == 3 then
			turtle.turnLeft()
		end
		face = 4
		while cx < x do
			accuMove("forward")
			cx = cx+1
		end
	elseif cx > x then
		if face == 1 then
			turtle.turnLeft()
		elseif face == 3 then
			turtle.turnRight()
		elseif face == 4 then
			turtle.turnLeft()
			turtle.turnLeft()
		end
		face = 2
		while cx>x do
			accuMove("forward")
			cx =  cx-1
		end
	end
	--Move in y axis
	if cy < y then
		if face == 2 then
			turtle.turnRight()
		elseif face == 3 then
			turtle.turnRight()
			turtle.turnRight()
		elseif face == 4 then
			turtle.turnLeft()
		end
		face = 1
		while cy<y do
			accuMove("forward")
			cy = cy+1
		end
	elseif cy > y then
		if face == 1 then
			turtle.turnLeft()
			turtle.turnLeft()
		elseif face == 2 then
			turtle.turnLeft()
		elseif face == 4 then
			turtle.turnRight()
		end
		face = 3
		while cy>y do
			accuMove("forward")
			cy = cy-1
		end
	end
	return x,y,z, face
end
