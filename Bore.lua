--Better Digger



local args = { ... }	--input arguments, expect width, height, depth, flags, keepList
local FLAGSIZE = 8
local width = args[1]
local height = args[2]
local depth = args[3]
local flags = args[4]
local keepList = args[5]
local x = 0
local y = 0
local z = -1

if #args ~= 6 then
	usage()
end

if #args ==6 then
	if(#args[4] ~= FLAGSIZE)
	usage()
end

--BE SURE TO UPDATE IF FLAGS ARE CHANGE
function usage()
	print("syntax: test width height depth flags keepList")
	print("Width, height, depth are self-explanatory.")
	print("flags an table of ".. FLAGSIZE.. " booleans listed below: ")
	print("1-5: wall placement:")
	print("top = 1")
	print("bot = 2")
	print("left = 3")
	print("right = 4")
	print("front = 5")
	print("back = 6")
	print("7: replace walls with inventory blocks")
	print("8: use only the block type in slot 1")
	print("Start with turtle facing bottom left corner of bore tunnel")
	print("All directions are with respect to facing this corner")
	print("keepList is a pairs table")

end

--Converts an integer value to binary
--then returns a table with corresponding booleans
--length of returned table will be integer multiple of 8
function bitToBool(number)
	local bit = select(2, math.frexp(number))	--selects the exponent return of  frexp, the length of the number in binary
	local padding = 8-bit%8
	bit = bit+padding     --Pad length to be divisible by 8 bits.

	--initialize with zeros
	local bintab = {}
	for i = 1,bit do
		bintab[i] = false
	end
	--start from LSB (
	while number > 0 do
		if number%2 == 1 then
			bintab[bit] = true
		end
		else
			bintab[bit] = false
		end
		bit = bit-1;
		number = math.floor(number/2)
	end
	return bintab
end

--[[MOVEMENT FUNCTIONS - USE THESE OVER TURTLE FUNCTIONS TO TRACK POSITION]]
function forward()

end

function back()
	turtle.back()

end

function turnLeft()

end

function turnRight()

end

function up()

end

function down()

end

function dig()
	turtle.dig()
end




--clears a single layer starting from in front of the turtle as the lower
--left corner, of dimensions height*width
--ends with turtle one step forward from beginning location
local function boreLayer()



end

--Dig the first row of the layer
function firstRow()
	turtle.dig()
	turtle.forward()

	turtle.turnRight()
	while x < width-1 do
		turtle.dig()
		turtle.forward()
		x = x + 1
	end
end

--expects turtle facing outer wall of frame
function upDigLeft()
	--move up and face inward
	turtle.digUp()
	turtle.up()
	turtle.turnLeft()
	turtle.turnLeft()

end

function upDigRight()

end
--return to origin

--clears a single block border surrounding a space of dimensions height*width
local function boreBorder()

end

--first clears the border of the height*width area
--next fills walls as specified by flags
local function replaceBorder()
	boreBorder()

end

--traverses the border of the height*width area, filling holes
--as specified by flags
local function patchBorder()


------------------------------------------------------------------------------
MAIN
------------------------------------------------------------------------------
--Case1: do not build walls
	--just bore the tunnel
--Case2: patch walls
	--bore the tunnel, patch on edges
--Case3: replace walls
	--bore the tunnel and an outer shell
	--For each layer, dig the outer shell as well, and replace

--For each case:
	--bore layer, then perform wall subroutine
	--if front wall is required
	--clear it first
		--if walls 7 is true, clear bore region plus shell
		--if walls 7 is false, clear bore region only
		--if walls 7 is true, fill shell with inventory material
			--if 8 is true, pull from slots other than 1 first
			--if only 1 piece in 1 (i.e. 1 remaining total) pause for reload
