--************ Print Coords *****************************************************************************
--* Print the blocks at each coordinate on an x,y grid with the 										*
--* turtle's current location as the origin, forward is positive y, right is positive x.				*
--* parameter is an array containing a list of x,y coordinates to print. Turtle will print in order.	*
--*******************************************************************************************************
function printCoords()

end

--************ Move To Coord*****************************************************************************
--* moves to coordinates 																				*
--* turtle's current location as the origin, forward is positive y, right is positive x.				*
--* parameter is an array containing a list of x,y coordinates to print. Turtle will print in order.	*
--*******************************************************************************************************

function moveTo()

end

function setOrigin()

end

function getCoords()

end