os.loadAPI("ocs/apis/sensor") -- load the sensor api
cropSensor = sensor.wrap("left") --create a sensor object, left is for turtles (put sensor in bottom right corner) otherwise use the side of a computer where you placed a sensor.

--NOTE: X/Z are horizontal, Y is vertical

--User entered farm size in x and y
ZUPPER = nil
XUPPER = nil

-- DO NOT MODIFY --
XLOWER = 1
ZLOWER = 1

turtlePos.X = 0
turtlePos.Y = 0
turtlePos.Z = 0
-- n,e,s,w
turtle.face = 'n'

destPos.X = 0
destPos.Y = 0
destPos.Z = 0

function calibrate()

orient = orientFromFile()

	if not orient then do
		print("What direction am I facing? (n,e,s,w): ")
			face = read()
			if  
			pos.face = read()
			else
			print
		while (not (face == n or face == e or face == s or face == w))
			print("Invalid, try lo
		end
		print("Current X:")
			pos.X = read()
		print("Current Z:")
			pos.Z = read()
		print("Current Y:")
			pos.Y = read()
	end

return pos
end

function moveOne(direction)
	move = false
	while move == false do
		if direction == 'f' then
			move = turtle.forward()
		elseif direction == 'd' then
			move = turtle.down()
		elseif direction == 'u' then 
			move = turtle.up()
		end
	end
	return move;
end

function moveTo(original, goal)
	current.X
	current.Y
	current.Z
	current.face
	continue
	
	--If there was an obstruction, print warning, TODO: IMPLEMENT LOG FILE
	
	--Move in z axis
	if(current.Z < goal.Z)
		while(current.Z < goal.Z)
			
			if not moveOne('u') then
				moveTo(current, original)
				print("Error in movement, aborted.")
				return false;
			else
				current.Z = current.Z + 1
			end
		end
	else
		while(current.Z > goal.Z)
			if not moveOne('d') then
				moveTo(current, original)
				print("Error in movement, aborted.")
				return false;
			else
				current.Z = current.Z - 1
			end
		end
	end
	--Move in X axis
	if(current.X < goal.X)
		while(current.X < goal.X)
			
			if not moveOne('u') then
				moveTo(current, original)
				print("Error in movement, aborted.")
				return false;
			else
				current.X = current.X + 1
			end
		end
	else
		while(current.X > goal.X)
			if not moveOne('d') then
				moveTo(current, original)
				print("Error in movement, aborted.")
				return false;
			else
				current.X = current.X - 1
			end
		end
	end
	--Move in y axis
	if(current.Y < goal.Y)
		while(current.Y < goal.Y)
			
			if not moveOne('u') then
				moveTo(current, original)
				print("Error in movement, aborted.")
				return false;
			else
				current.Y = current.Y + 1
			end
		end
	else
		while(current.Y > goal.Y)
			if not moveOne('d') then
				moveTo(current, original)
				print("Error in movement, aborted.")
				return false;
			else
				current.Y = current.Y - 1
			end
		end
	end
end





if(not YUPPER or not XUPPER) then
	print("Please set the X and Y upper limits in the program file.")
	break
end

function moveTo(current, goal)
	
	if(current.X > goal.X)
	else
	end
	if(current.Y > goal.Y)
	else
	end
	

	newPos.X
	newPos.Y
	newPos.Z

return nowPos	
end



--just a test function prior to making the navigation functions required to control the turtle
function harvest(coords)
	print("Harvesting at: "..coords["x"]..","..coords["y"]..","..coords["z"])
end


--Will loop indefinitely. It waits 5 minutes between cycles.
while true do
	crops = cropSensor.getTargets()
	detailTable = {}
	
	-- crops is a dictionary , name is just a concatenation of coordinates for the block. details is another dictionary with basic details that I don't need for this.
	for name,details in pairs(crops) do
		extraInfo = cropSensor.getTargetDetails(name)
		--extraInfo is a dictionary, important keys are "Position" "Status" and possibly "Name" if you want to do something different per crop type.
		if extraInfo["Status"] == "Grown" 
			then harvest(extraInfo["Position"])
		end
	end
	--return to initial position(will prevent wandering)
	returnToHome()
	os.sleep(300)
end