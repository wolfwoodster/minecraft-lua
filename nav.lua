--nav.lua

--------------------------------------------------------------------------------
--This module is intended to be included with turtle programs requiring relative
--coordinate based movement.
--usage:
--at the start of your program, do: nav = require 'nav'
--nav.coords returns a table containing x,y,z coordinates in pairs
--for example, x = nav.coords.x places the current x coordinate in x
--available functions:
--forward, back, turnLeft, turnRight, up, down
--calling any of these functions will modify relative coordinates and/or facing
--appropriately
--Movement commands are
--CAUTION: CALLING A MOVEMENT COMMAND NOT FROM THIS MODULE WILL CAUSE YOUR
--TURTLE TO GET LOST AS COORDINATES AND FACING WILL NOT BE UPDATED
--------------------------------------------------------------------------------

--coordinates are as below. Turtle starts at 0,0 facing positive y
--y            y
--          -
--       -
--    -
-- -
---------------x
--------------------------------------------------------------------------------
local M = {}

--Integer value
-- y = 0 ; -y = 1 ; x = 2 ; -x = 3
M.face  = "y"
M.coords = {x = 0; y = 0; z = 0}

function M.changeCoords()
  M.coords.x = 5
  M.coords.y = 3
  M.coords.z = 6
end

return M
